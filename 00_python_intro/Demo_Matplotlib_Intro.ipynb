{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization with Matplotlib\n",
    "\n",
    "Matplotlib (www.matplotlib.org) is the standard plotting library for Python. It is very powerful for customized, high-quality 2D graphics and plays well with the notebook. It is not so strong for 3D and interactive web-based visualizations. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The import convention is as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "plt.style.use(\"seaborn-notebook\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each matplotlib plot consists of a figure and axes (aka. subfigures)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "ax = plt.axes()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Matplotlib, the *figure* (an instance of the class ``plt.Figure``) can be thought of as a single container that contains all the objects representing axes, graphics, text, and labels.\n",
    "The *axes* (an instance of the class ``plt.Axes``) is what we see above: a bounding box with ticks and labels, which will eventually contain the plot elements that make up our visualization.\n",
    "Throughout this book, we'll commonly use the variable name ``fig`` to refer to a figure instance, and ``ax`` to refer to an axes instance or group of axes instances.\n",
    "\n",
    "Once we have created an axes, we can use the ``ax.plot`` function to plot some data. Let's start with a simple sinusoid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "x = np.linspace(0, 10, 1000)\n",
    "ax.plot(x, np.sin(x))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The approach just described can become quite tedious when creating a large grid of subplots, especially if you'd like to hide the x- and y-axis labels on the inner plots.\n",
    "For this purpose, ``plt.subplots()`` is the easier tool to use (note the ``s`` at the end of ``subplots``). Rather than creating a single subplot, this function creates a full grid of subplots in a single line, returning them in a NumPy array.\n",
    "The arguments are the number of rows and number of columns, along with optional keywords ``sharex`` and ``sharey``, which allow you to specify the relationships between different axes.\n",
    "\n",
    "Here we'll create a $2 \\times 3$ grid of subplots, where all axes in the same row share their y-axis scale, and all axes in the same column share their x-axis scale:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(2,3)\n",
    "ax[0,0].plot(x, np.sin(x))\n",
    "ax[1,0].plot(x, np.cos(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(2, 3, sharex='col', sharey='row')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# axes are in a two-dimensional array, indexed by [row, col]\n",
    "for i in range(2):\n",
    "    for j in range(3):\n",
    "        ax[i, j].text(0.5, 0.5, str((i, j)),\n",
    "                      fontsize=18, ha='center')\n",
    "fig"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A complete example of a figure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.arange(0, 6 * np.pi, 0.1)\n",
    "\n",
    "fig, axes = plt.subplots(1, 2, figsize=(14 / 2.54, 7 / 2.54), sharey=True, dpi=150)\n",
    "\n",
    "ax = axes[0]\n",
    "ax.plot(x, np.sin(x))\n",
    "ax.set_xlabel('Angular frequency [Hz]')\n",
    "ax.set_ylabel('sin(x)')\n",
    "ax.set_title('Sine')\n",
    "\n",
    "ax = axes[1]\n",
    "ax.plot(x, np.cos(x), '.-', color='r')\n",
    "\n",
    "ax.plot(x, np.sin(x + np.pi / 2.0), color='c')\n",
    "\n",
    "ax.axhline(y=0)\n",
    "\n",
    "ax.fill_between(x,\n",
    "    y1=x * 0,\n",
    "    y2=np.cos(x),\n",
    "    hatch='/',\n",
    "    alpha=0.6,\n",
    "    color='gray',\n",
    ")\n",
    "ax.set_xlabel('Angular frequency [Hz]')\n",
    "ax.set_ylabel('$\\Omega \\sim \\int$')\n",
    "ax.set_title('Cosine')\n",
    "\n",
    "for ax, label in zip(axes, \"ab\"):\n",
    "    ax.set_xlim(0, 6 * np.pi)\n",
    "    ax.set_title(label + \")\", loc=\"left\")\n",
    "    \n",
    "fig.savefig(\"myfigure.pdf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Customization is endless, if you know the [anatomy of a figure](https://matplotlib.org/gallery/showcase/anatomy.html?highlight=anatomy%20figure)\n",
    "\n",
    "![anatomy](https://matplotlib.org/_images/anatomy.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
