#!/usr/bin/env python
# coding: utf-8

# # Solution to Exercise 1 (Nov. 3 & 10, 2020)

# In[2]:


# Importing numpy for calculations and matplotlib for plotting
import numpy as np
import matplotlib.pyplot as plt


# In the following exercises, you will perform curve fitting exercises of increasing complexity. Curve fitting, so finding mathematical model parameters that best describe given data, represents an inverse problem. While several implementations for regression problems are available in Python libraries such as Numpy's [polyfit](https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html) or [linear algebra solver](https://numpy.org/doc/stable/reference/generated/numpy.linalg.solve.html?highlight=solve#numpy.linalg.solve), please solve the exercises by calculating the appropriate generalized inverse as introduced in the lecture. 
# 
# For this, you will need to perform [matrix inversions](https://numpy.org/doc/stable/reference/generated/numpy.linalg.inv.html?highlight=inv#numpy.linalg.inv), [matrix transposes](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.T.html#numpy.ndarray.T), as well as matrix-matrix and matrix-vector [multiplications](https://numpy.org/doc/stable/reference/generated/numpy.matmul.html?highlight=matmul#numpy.matmul). Note that the matrix-multiplication function `C = np.matmul(A,B)` can also be performed by the `@`-operator, e.g. `C = A @ B`. Using the multiplication sign (`*`) on two matrices will not return their product, but will perform element-wise multiplication instead. You can also multiply an array with a scalar to multiply each individual element by that value.

# # 1. Linear Models

# ## 1.1 Fitting a linear model with few observations

# Assume we have two measurements $d_1$ and $d_2$, that were taken from a linear process that can be described by $d(x) = a \cdot x + b$:
# 
# $d_1 = 13$ and $d_2 = 7$, taken at $x_1 = 10$ and $x_2 = 15$
# 
# (a) Create vectors for data and x-values, and visualize the two datapoints in a [scatterplot](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.pyplot.scatter.html):

# (b) Which type of problem is posed here? Which solution is appropriate to solve the inverse problem?

# (c) What are the dimensions of the $\mathbf{G}$ matrix? Write it down on paper first and then construct it into a numpy array.

# (d) Using the appropriate solution from questions (b), estimate the model parameters $a$ and $b$ given the observations.

# Check your solution for a and b by using them to get a data estimate at $x = 10$ and $x = 15$

# (e) Visualize a range of model realizations from $x = -50$ to $x = 50$ and plot the original data against it. We can use Numpy's [linspace](https://numpy.org/doc/stable/reference/generated/numpy.linspace.html) function to create an auxiliary variable x.

# ## 1.2 Fitting a linear model with many observations

# We have given 25 data points of a hypothetical measurement taken at 25 different positions $x$ of a similar linear process as in the previous task:

# In[8]:


d1 = np.array([  7.93494028,  5.68739874,  2.6285696 , 12.21816911, 11.94169231,
                 4.13625885,  8.51225942,  5.62103488,  5.65592905, 22.08629139,
                12.44603838, 18.49236748, 17.7113576 , 17.360048  , 12.79941666,
                13.92529062, 22.1420152 , 26.53605093, 12.65966116, 18.11449018,
                22.75746405, 29.95185639, 28.37414316, 30.70226002, 28.8055315 ])

x1 = np.linspace(0,10,25)


# (a) What type of problem is presented here? Which solution is appropriate to solve it?
# 

# (b) Explicitly write down $\mathbf{G}$ and create it as numpy array.

# (c). Estimate the model parameters and put this into a function for later reuse.

# (d) Visualize the measured data together with the predicted data $\mathbf{d}_\text{pre} = \mathbf{Gm}_\text{est}$ and again put this into a function for later reuse.

# (e) Compute the RMS ("root mean square") error between the given observations and the model response, i.e. the predicted data.
# $$RMSE = \sqrt{\frac{1}{N} \sum_{i=1}^N ( d^i - d^i_\mathrm{pre}))^2 }$$
# 
# Again, put this into a function.

# # 2. Fitting a parabolic model

# The general form of a parabola can be written as $y(x) = a + b \cdot x + c \cdot x^2.$
# 
# Given are 25 measurements of a parabolic process:

# In[15]:


np.random.seed(23)
x2 = np.linspace(-15, 5, 25)
d_orig = -1.2 + 9.3 * x2 + 1.2 * x2**2
d2 = d_orig + np.random.normal(scale=5, size=25)


# (a) Construct the forward operator (i.e., the $\mathbf{G}$ matrix).

# (b) Estimate $\mathbf{m}_\mathrm{est}$ for the given data.

# (c) Plot measurement data and predicted data.

# ---
# ## Bonus questions:
# 
# ### B1) What happens if you try to solve the problem above only using the first and the last measurement?

# ### B2) The effect of data noise.
# 
# Revisit the first exercise (i.e., the linear fit). We can also create our own, synthetic measurement data and check our model predictions for different noise nevels. We do this by first creating noise-free data, and then adding randomly distributed noise drawn from Numpy's [random](https://numpy.org/doc/stable/reference/random/generated/numpy.random.normal.html?highlight=rand%20normal#numpy.random.normal) module. 
# 
# First, create noise-free, synthetic data for the process $d(x) = 2.1*x + 5$. If you reuse your last x-vector for this, you can also reuse your last G-Matrix.
# 
# Put (c)-(e) in a function, which can take an argument for the noise component. Visualize the synthetic and predicted data for 3 different noise levels on your synthetic data and put the RMS in the title of the respective plot.

# In[13]:


d_orig = 2.1 * x1 + 5

